import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadingStrategy, PreloadAllModules } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { assetResolver } from './resolvers/assets-resolver.resolver';

const routes: Routes = [
  {
    path: '', pathMatch: 'full',  component: MainPageComponent, 
    //resolve: { message: assetResolver }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }


//TODO: don't reload page if "BACK" used