import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MainPageComponent } from './main-page/main-page.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { UserSelectFormComponent } from './user-select-form/user-select-form.component';
import { ThreejsbackgroundComponent } from './threejsbackground/threejsbackground.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    LoadingScreenComponent,
    UserSelectFormComponent,
    ThreejsbackgroundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
