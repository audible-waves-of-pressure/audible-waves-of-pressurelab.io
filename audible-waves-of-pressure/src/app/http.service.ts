import { Injectable } from '@angular/core';
import { HttpClient, HttpResponseBase, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(private http: HttpClient) { }

  readonly url: string = "https://ws.audioscrobbler.com/2.0/";
  readonly key: string = "13f6eac68537244d72d36f5ef09f31e9";
  readonly api_key: string = "&api_key=" + this.key;
  readonly json: string = "json";
  readonly format: string = "&format=" + this.json;
  readonly me: string = "awesor";
  readonly user: string = "&user=" + this.me;

  testMethod() {
    return console.log("what's up");
  }

  getUserInfo(): Observable<Object>;
  getUserInfo(user: string): Observable<Object>;
  getUserInfo(user?: string) {
    if (typeof user !== "undefined") {
      user = "&user=" + user;
    } else {
      user = this.user;
    }
    return this.http.get(this.url + "?method=user.getinfo" + user + this.api_key + this.format);
  }

  getTopTrack(): Observable<Object>;
  getTopTrack(user: string): Observable<Object>;
  getTopTrack(user?: string) {
    if (typeof user !== "undefined") {
      user = "&user=" + user;
    } else {
      user = this.user;
    }
    return this.http.get(this.url + "?method=user.gettoptracks" + user + "&period=7day&limit=1" + this.api_key + this.format);
  }
  getTopArtists(): Observable<Object>;
  getTopArtists(user: string): Observable<Object>;
  getTopArtists(user?: string){
    if (typeof user !== "undefined") {
      user = "&user=" + user;
    } else {
      user = this.user;
    }
    return this.http.get(this.url + "?method=user.gettopartists" + user + "&limit=10&period=3month" + this.api_key + this.format);
  }

  getTopTagsForArtist(artist: string): Observable<Object> {
    return this.http.get(this.url + "?method=artist.gettoptags&artist=" + artist + this.api_key + this.format);
  }

  //1 month top tracks
  //http://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=awesor&api_key=  KEY  &period=1month&format=json

  //first top track 1month 
  //http://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=awesor&api_key=  KEY  &period=1month&limit=1&format=json

  //top artists for 3 month
  //http://ws.audioscrobbler.com/2.0/?method=user.gettopartists&user=awesor&api_key=  KEY  &period=3month&format=json

  //artist top tags
  //http://ws.audioscrobbler.com/2.0/?method=artist.getTopTags&artist=Placebo&user=awesor&api_key=  KEY  &format=json
}

