import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Observable, forkJoin, from } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})

export class MainPageComponent implements OnInit {

  
  userInfo: Object;
  date: number;
  topTrack: Object;
  topArtists: Object;
  genres: Array<string> = [];
  genresCount: Array<Object>;
  //user: string;
  canShow: boolean = false;
  valueEmittedFromChildComponent: string = '';


  constructor(private _http: HttpService, private router: Router, private route: ActivatedRoute) {   }

  ngOnInit(): void {
    let currentUsername: string = "";
    this.route.queryParams.subscribe(params => {
        let username = params['username'];
        if (username == "") {
          currentUsername = "Awesor";
        } else {
          currentUsername = username;
        }
        this.loadData(currentUsername);
    });
  }

  loadData(currentUsername: string):void {
    //console.log(typeof this._http.getUserInfo().subscribe())
    this._http.getUserInfo(currentUsername).subscribe(
    data => {
      this.userInfo = undefined;
      this.userInfo = data['user'];
      this.date = undefined;
      this.date = data['user']['registered']['#text'] * 1000;
      //console.log(data['user']['image'][2]['#text']);
      if (data['user']['image'][2]['#text'] == "") {
        data['user']['image'][2]['#text'] = "../../assets/img/no-profile-pic.svg"
      }
      //console.log("userInfo");
      //console.log(this.userInfo);
    },
    error => console.error('There was an error!', error)
    );
    this._http.getTopTrack(currentUsername).subscribe(
    data => {
      this.topTrack = undefined;
      this.topTrack = data['toptracks']['track']['0'];
      // console.log("topTrack");
      // console.log(this.topTrack);
    },
    error => console.error('There was an error!', error)
    );
    this._http.getTopArtists(currentUsername).subscribe(
    data => {
      this.topArtists = undefined;
      if (data['topartists']['artist']['length']!==0){
        
        this.topArtists = data['topartists']['artist'];
        //console.log(this.topArtists);
        let observables: Observable<any>[] = [];
        let x: any;
        for (x of data['topartists']['artist']) {
            observables.push(this._http.getTopTagsForArtist(x['name']))
        }

        forkJoin(observables)
          .subscribe(dataArray => {
            this.genres = [];
            // console.log(dataArray);
            for(x of dataArray) {
              // console.log('===' + x['toptags']['@attr']['artist'] + "===");
              // console.log(x['toptags']['tag']);
              let l = 10;
              if (x['toptags']['tag']['length']<10) {
                l = x['toptags']['tag']['length'];
              }
              for (let y = 0; y < l; y++) {
                // console.log(x['toptags']['tag'][y]['name']);
                this.genres.push(x['toptags']['tag'][y]['name']);
              }
            }
            //console.log(this.genres);
            let genresCountObject: Object = {};
            this.genres.forEach(
              element => {
                genresCountObject[element] = (genresCountObject[element] || 0)+1; 
              }
            );
            this.genresCount = undefined;
            this.genresCount = Object.entries(genresCountObject)
              .sort(function(a, b){return b[1]-a[1]})
              .filter(val => val[0] != "seen live" && val[0] != "british" && val[0] != "electronica" 
              && val[0] != "alternative rock" && val[0] != "indie rock" );
            //console.log(this.genresCount);

            this.canShow = true;
        });
      } else {
        this.canShow = true;
      }

    },
    error => console.error('There was an error!', error)
    );
    //console.log(this.topArtists);
  }

  //TODO: fix user not found
}
