import { TestBed } from '@angular/core/testing';

import { PreloadAssetsService } from './preload-assets.service';

describe('PreloadAssetsService', () => {
  let service: PreloadAssetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreloadAssetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
