import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreloadAssetsService {

  private assets: Array<string> = 
  [ '/assets/fonts/Oswald-ExtraLight.woff2',
    '/assets/fonts/Oswald-Medium.woff2',
    '/assets/img/wavemesh.svg']

  constructor(private http: HttpClient) { }

  loadAssets() {
    let observables: Observable<any>[] = [];
    let options: object = {responseType: "blob"};
    for(let asset of this.assets) {
      console.log(asset);
      observables.push(this.http.get(asset, options));
    }
    return forkJoin(observables);
  }
}
