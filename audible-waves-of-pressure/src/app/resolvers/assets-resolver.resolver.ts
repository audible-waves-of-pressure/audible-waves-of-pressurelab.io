import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { PreloadAssetsService } from '../preload-assets.service'

@Injectable({
  providedIn: 'root'
})

export class assetResolver implements Resolve<any> {
  constructor(private preloadAssetsService: PreloadAssetsService) {}

  resolve() {
    console.log('resolving');
    return this.preloadAssetsService.loadAssets();
  }
}