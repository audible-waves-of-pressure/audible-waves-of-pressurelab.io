import * as THREE from 'three';
import {Injectable, ElementRef, OnDestroy, NgZone} from '@angular/core';
import Stats from 'three/examples/jsm/libs/stats.module';
import {GUI} from 'three/examples/jsm/libs/dat.gui.module.js';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js';
import {FirstPersonControls} from 'three/examples/jsm/controls/FirstPersonControls.js';

@Injectable({providedIn: 'root'})
export class EngineService implements OnDestroy {
    private canvas : HTMLCanvasElement;
    private renderer : THREE.WebGLRenderer;
    private camera : THREE.PerspectiveCamera;
    private scene : THREE.Scene;

    private frameId : number = null;

    private pointSize : number = 0.035;

    private SEPARATION = 0.45; 
    private AMOUNTX = 200;
    private AMOUNTY = 200;
    private particles: THREE.Points;
    private count: number = 0;

    private mouseX: number = 0;
    private mouseY: number = 0;
    private windowHalfX: number = window.innerWidth / 2;
    private windowHalfY: number = window.innerHeight / 2;
    
    private stats : Stats;

    private gui : any;


    public constructor(private ngZone : NgZone) {}

    public ngOnDestroy(): void {
        if (this.frameId != null) {
            cancelAnimationFrame(this.frameId);
        }
    }

    public createScene(canvas : ElementRef < HTMLCanvasElement >): void { // The first step is to get the reference of the canvas element from our HTML document
        this.canvas = canvas.nativeElement;

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvas, alpha: true, // transparent background
            antialias: true, // smooth edges
        });
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setPixelRatio(window.devicePixelRatio);

        // create the scene
        this.scene = new THREE.Scene();

        // create fog
        let fogColor = new THREE.Color(0x000000);
        this.scene.fog = new THREE.Fog(fogColor, 0.0025, 70);

        this.camera = new THREE.PerspectiveCamera(110, window.innerWidth / window.innerHeight, 10, 70);
        this.camera.position.set( 0, 0, 40 );
        this.scene.add(this.camera);

        this.particles = this.generatePointcloud(this.AMOUNTX,this.AMOUNTY);
        this.particles.position.set( 0, 0, 0 );
        let deg = THREE.MathUtils.degToRad(-225);
        this.particles.rotation.set( 0, deg, 0);
        this.scene.add( this.particles );

        // stats
        /*this.stats = Stats();
        this.canvas.parentElement.append(this.stats.domElement);*/
    }

    public animate(): void {
        // We have to run this outside angular zones,
        // because it could trigger heavy changeDetection cycles.
        this.ngZone.runOutsideAngular(() => {
            if (document.readyState !== 'loading') {
                this.render();
            } else {
                window.addEventListener('DOMContentLoaded', () => {
                    this.render();
                });
            }

            //this.buildGui();

            window.addEventListener('resize', () => {
                this.resize();
            });

            document.addEventListener('mousemove', (e) => {
              this.onDocumentMouseMove(e);
            })

        });


    }

    public render(): void {
        this.frameId = requestAnimationFrame(() => {
            this.render();
        });

        this.camera.position.x += ( this.mouseX - this.camera.position.x ) * .05 ;
        this.camera.position.y += ( - this.mouseY - this.camera.position.y ) * .05 ;
        this.camera.lookAt( this.scene.position );

        let positions = this.particles.geometry["attributes"].position.array;

				let i = 0, j = 0;

				for ( let ix = 0; ix < this.AMOUNTX; ix ++ ) {

					for ( let iy = 0; iy < this.AMOUNTY; iy ++ ) {
						//positions[ i + 1 ] = ( Math.sin( ( ix + this.count ) * 0.3 ) * 50 ) + ( Math.sin( ( iy + this.count ) * 0.5 ) * 50 );
						positions[ i +1 ] = ( Math.sin( ( ix + this.count ) * 0.100 ) * 3 ) + ( Math.sin( ( iy + this.count ) * 0.100 ) * 3 );

						i += 3;
						j ++;
					}

				}
				this.particles.geometry["attributes"].position.needsUpdate = true;
        this.count += 0.12;

        this.renderer.render(this.scene, this.camera);
        //this.stats.update();
        // this.controls.update();
        //this.controlsFP.update(this.clock.getDelta());
    }

    public resize(): void {
        const width = window.innerWidth;
        const height = window.innerHeight;

        this.windowHalfX = window.innerWidth / 2;
        this.windowHalfY = window.innerHeight / 2;
        
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(width, height);
    }

    generatePointCloudGeometry(AMOUNTX: number, AMOUNTY: number): THREE.BufferGeometry {
      let numParticles = AMOUNTX * AMOUNTY;
      let positions = new Float32Array(numParticles * 3);
      let i = 0, j = 0;

      for (let ix = 0; ix < this.AMOUNTX; ix++) {
          for (let iy = 0; iy < this.AMOUNTY; iy++) {
              positions[i] = ix * this.SEPARATION -((this.AMOUNTX * this.SEPARATION) / 2); // x
              positions[i + 1] = 0; // y
              positions[i + 2] = iy * this.SEPARATION -((this.AMOUNTY * this.SEPARATION) / 2); // z

              i += 3;
              j++;
          }
      }

      let geometry = new THREE.BufferGeometry();
      geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));

      return geometry;
    }

    generatePointcloud(width: number, length: number): THREE.Points {
      let geometry = this.generatePointCloudGeometry(width, length);
      let material = new THREE.PointsMaterial({size: this.pointSize, vertexColors: false});
      return new THREE.Points(geometry, material);
    }

    buildGui(): void {
        this.gui = new GUI();

        var params = {
            //wavesHeight: this.wavesHeight,
        };

        this.gui.add(params, 'wavesHeight', 0, 1000).onChange((val) => {
            //this.wavesHeight = val;
        });

        this.gui.open();
    }

    onDocumentMouseMove( event: MouseEvent ): void {
      this.mouseX = (event.clientX - this.windowHalfX) /200;
      this.mouseY = (event.clientY - this.windowHalfY) /200;
    }


}
