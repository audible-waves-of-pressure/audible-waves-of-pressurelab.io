import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreejsbackgroundComponent } from './threejsbackground.component';

describe('ThreejsbackgroundComponent', () => {
  let component: ThreejsbackgroundComponent;
  let fixture: ComponentFixture<ThreejsbackgroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreejsbackgroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreejsbackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
