import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { EngineService } from './engine.service';

@Component({
  selector: 'threejsbackground',
  templateUrl: './threejsbackground.component.html',
  styleUrls: ['./threejsbackground.component.scss']
})
export class ThreejsbackgroundComponent implements OnInit {

  @ViewChild('rendererCanvas', {static: true})
  public rendererCanvas: ElementRef<HTMLCanvasElement>;

  constructor(private engServ: EngineService) { }

  ngOnInit(): void {
    this.engServ.createScene(this.rendererCanvas);
    this.engServ.animate();
  }

}