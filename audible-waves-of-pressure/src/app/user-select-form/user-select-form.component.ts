import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'user-select-form',
  templateUrl: './user-select-form.component.html',
  styleUrls: ['./user-select-form.component.scss']
})
export class UserSelectFormComponent implements OnInit {

  userSelect: FormGroup;
  placeholder:string = "username";
  invisibleSpanValue:string = "";

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.userSelect = this.fb.group(
      {
        username: '',
      }
    )
    this.userSelect.valueChanges.subscribe(console.log);
  }

}
